# Example of TODO.md

This is an example of TODO.md

View the raw content of this file to understand the format.

### Todo

- [ ] Codebase – one codebase tracked in revision control, many deploys
  - [ ] separate repositories for each microservice (1)
  - [ ] continuous integration to build, test and create the artefact (3)
  - [ ] implement some tests and test each service separately (unit tests, integration tests) (5)
- [ ] Dependencies – explicitly declare and isolate dependencies
  - [ ] preferably Maven project with pom.xml
  - [ ] eventually gradle project or other
- [ ] Config
  - [ ] configuration of services provided via environmental properties (1)
  - [ ] eventually as configuration as code (bonus: 0.5)
- [ ] Backing services – treat backing services as attached resources
  - [ ] backing services like database and similar will be deployed as containers too (1)
- [ ] Build, release, run – strictly separate build and run stages
  - [ ] CI & docker
  - [ ] eventually upload your images to docker hub (bonus: 1)
- [ ] Processes – execute the app as one or more stateless processes (1)
- [ ] Port binding – export services via port binding (1)
- [ ] Disposability – maximize robustness with fast startup and graceful shutdown
  - [ ] ability to stop/restart service without catastrophic failure for the rest (2)
- [ ] Dev/prod parity – keep development, staging, and production as similar as possible
  - [ ] repository for integration testing and system demonstration (2)
  - [ ] services will be deployed as containers
- [ ] Logs – treat logs as event streams
  - [ ] log into standard output (1)
  - [ ] eventually collect logs in Elastic (bonus: 0.5)
- [ ] Communication
  - [ ] REST API defined using Open API standard (Swagger) (2)
  - [ ] auto-generated in each service (1)
  - [ ] clear URLs (2)
  - [ ] clean usage of HTTP statuses (2)
  - [ ] eventually message based asynchronous communication via queue (bonus: 1)
- [ ] Transparency – the client should never know the exact location of a service.
  - [ ] service discovery (2)
  - [ ] eventually client side load balancing (bonus: 0.5) or workload balancing (bonus: 0.5)
- [ ] Health monitoring – a microservice should communicate its health
  - [ ] Actuators (1)
  - [ ] eventually Elastic APM (bonus: 1)
- [ ] Design patterns – use the appropriate patterns (2)
- [ ] Scope – use domain driven design or similar to design the microservices (5)
- [ ] Documentation – visually communicate architecture of your system (5)

### Done ✓

- [x] Nothing for now
